// Copyright (c) 2020, Oracle and/or its affiliates.
// Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl.

// Code generated by lister-gen. DO NOT EDIT.

package v1beta1

// CohClusterListerExpansion allows custom methods to be added to
// CohClusterLister.
type CohClusterListerExpansion interface{}

// CohClusterNamespaceListerExpansion allows custom methods to be added to
// CohClusterNamespaceLister.
type CohClusterNamespaceListerExpansion interface{}
