// Copyright (c) 2020, Oracle and/or its affiliates.
// Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl.

package cohcluster

import (
	"context"
	"fmt"
	"os/exec"
	"reflect"
	"time"

	v8ov1beta1 "github.com/verrazzano/verrazzano-coh-cluster-operator/pkg/apis/verrazzano/v1beta1"
	"go.uber.org/zap"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

// Add creates a new CohCluster Controller and adds it to the Manager. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileCohCluster{client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("cohcluster-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to primary resource CohCluster
	err = c.Watch(&source.Kind{Type: &v8ov1beta1.CohCluster{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	return nil
}

// blank assignment to verify that ReconcileCohCluster implements reconcile.Reconciler
var _ reconcile.Reconciler = &ReconcileCohCluster{}

// ReconcileCohCluster reconciles a CohCluster object
type ReconcileCohCluster struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	client client.Client
	scheme *runtime.Scheme
}

const finalizer = "cohclusters.verrazzano.oracle.com"

// Reconcile reads that state of the cluster for a CohCluster object and makes changes based on the state read
// and what is in the CohCluster.Spec
// Note:
// The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcileCohCluster) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	// create reqlogger for reconciliation
	reqLogger := zap.S().With("Request.Namespace", request.Namespace, "Request.Name", request.Name)
	reqLogger.Infow("Reconciling CohCluster")

	// Fetch the CohCluster instance
	instance := &v8ov1beta1.CohCluster{}
	err := r.client.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		// If the resource is not found, that means all of
		// the finalizers have been removed, and the CohCluster
		// resource has been deleted, so there is nothing left
		// to do.
		if errors.IsNotFound(err) {
			return reconcile.Result{}, nil
		}

		// Error reading the object - requeue the request.
		reqLogger.Errorf("Failed to get CohCluster, Error: %s", err)
		return reconcile.Result{}, err
	}

	// Check if the CohCluster CR is marked to be deleted, which is
	// indicated by the deletion timestamp being set.
	isMarkedToBeDeleted := instance.GetDeletionTimestamp() != nil
	if isMarkedToBeDeleted {
		if contains(instance.GetFinalizers(), finalizer) {
			// Run finalization logic for the finalizer. If the
			// finalization logic fails, don't remove the finalizer so
			// that we can retry during the next reconciliation.
			if err := r.runFinalizer(reqLogger, instance); err != nil {
				return reconcile.Result{}, err
			}

			// Remove finalizer. Once all finalizers have been
			// removed, the object will be deleted.
			instance.SetFinalizers(remove(instance.GetFinalizers(), finalizer))
			err := r.client.Update(context.TODO(), instance)
			if err != nil {
				return reconcile.Result{}, err
			}
		}
		return reconcile.Result{}, nil
	}

	// Check if the namespace for the Coherence operator exists, if not found create it
	// Define a new Namespace object
	namespaceFound := &corev1.Namespace{}
	reqLogger.Infow("Checking if namespace exist")
	err = r.client.Get(context.TODO(), types.NamespacedName{Name: instance.Spec.Namespace}, namespaceFound)
	if err != nil && errors.IsNotFound(err) {
		reqLogger.Infof("Creating a new namespace, Namespace: %s", instance.Spec.Namespace)
		err = r.client.Create(context.TODO(), newNamespace(instance))
		if err != nil {
			return reconcile.Result{}, err
		}

		// Namespace created successfully - return and requeue
		return reconcile.Result{Requeue: true}, nil
	} else if err != nil {
		return reconcile.Result{}, err
	}

	// Check if the Coherence operator deployment already exists, if not create a new one via Helm
	reqLogger.Infow("Checking if deployment exists")
	deploymentFound := &appsv1.Deployment{}
	err = r.client.Get(context.TODO(), types.NamespacedName{Name: instance.Spec.Name, Namespace: instance.Spec.Namespace}, deploymentFound)
	if err != nil && errors.IsNotFound(err) {
		// Create the helm arg list for deploying the Coherence operator
		args := getCreateArgs(instance)

		// Execute a Helm install command to deploy the Coherence operator
		reqLogger.Infof("Deploying Coherence operator with helm, Args: %s", args)
		cmd := exec.Command("helm", args...)
		out, err := cmd.CombinedOutput()
		if err != nil {
			reqLogger.Errorf(string(out)+", Error: %s", err.Error())
			_ = r.updateStatus(reqLogger, instance, "Failed", "Coherence operator deployment failed: "+string(out))
			return reconcile.Result{}, err
		}

		_ = r.updateStatus(reqLogger, instance, "Deployed", "Coherence operator deployed successfully")

		// Add finalizer for this CR since at this point the Coherence operatorhas been deployed
		if !contains(instance.GetFinalizers(), finalizer) {
			if err := r.addFinalizer(reqLogger, instance); err != nil {
				return reconcile.Result{}, err
			}
		}

		// Deployment created successfully - don't requeue
		return reconcile.Result{}, nil
	} else if err != nil {
		reqLogger.Errorf("Failed to get Coherence operator deployment, Error: %s", err.Error())
		return reconcile.Result{}, err
	}

	// Create the helm arg list for upgrading the Coherence operator
	args, err := r.getUpgradeArgs(reqLogger, instance, deploymentFound)
	if err != nil {
		return reconcile.Result{}, err
	}
	if args != nil {
		// Execute the Helm update command for any updates needed
		reqLogger.Infof("Upgrading Coherence operator with helm, Args: %s", args)
		cmd := exec.Command("helm", args...)
		out, err := cmd.CombinedOutput()
		if err != nil {
			reqLogger.Errorf(string(out)+", Error: %s", err.Error())
			_ = r.updateStatus(reqLogger, instance, instance.Status.State, "Coherence operator deployment update failed: "+string(out))
			return reconcile.Result{}, err
		}

		_ = r.updateStatus(reqLogger, instance, "Updated", "Coherence operator deployment updated")

		// Deployment updated successfully - don't requeue
		return reconcile.Result{}, nil
	}

	// Coherence deployment already exists - don't requeue
	reqLogger.Infof("Skip reconcile: Coherence deployment already exists, Name: %s Namespace: %s", instance.Spec.Name, instance.Spec.Namespace)
	return reconcile.Result{}, nil
}

// createNamespace returns a namespace resource that may need to be created
func newNamespace(cr *v8ov1beta1.CohCluster) *corev1.Namespace {
	namespace := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: cr.Spec.Namespace,
		},
	}

	return namespace
}

// Return list of helm args needed for creating the Coherence operator
func getCreateArgs(cr *v8ov1beta1.CohCluster) []string {
	var args []string
	args = append(args, "install", cr.Spec.Name, "resources/charts/coherence-operator",
		"--namespace", cr.Spec.Namespace,
		"--set", fmt.Sprintf("serviceAccount=%s", cr.Spec.ServiceAccount))

	// Add the optional imagePullSecret
	if len(cr.Spec.ImagePullSecrets) != 0 {
		for i, secret := range cr.Spec.ImagePullSecrets {
			args = append(args, "--set", fmt.Sprintf("imagePullSecrets[%d].name=%s", i, secret.Name))
		}
	}

	return args
}

// Return list of helm args needed for upgrading the Coherence operator
func (r *ReconcileCohCluster) getUpgradeArgs(reqLogger *zap.SugaredLogger, cr *v8ov1beta1.CohCluster, deploymentFound *appsv1.Deployment) ([]string, error) {
	var args []string
	args = append(args, "upgrade", cr.Spec.Name, "resources/charts/coherence-operator", "--reuse-values",
		"--namespace", cr.Spec.Namespace)
	fixedArgs := len(args)

	// Check if imagePullSecrets needs updating
	if !reflect.DeepEqual(deploymentFound.Spec.Template.Spec.ImagePullSecrets, cr.Spec.ImagePullSecrets) {
		for i, secret := range cr.Spec.ImagePullSecrets {
			args = append(args, "--set", fmt.Sprintf("imagePullSecrets[%d].name=%s", i, secret.Name))
		}
	}

	if len(args) > fixedArgs {
		return args, nil
	} else {
		return nil, nil
	}
}

// Update the status for the CR
func (r *ReconcileCohCluster) updateStatus(reqLogger *zap.SugaredLogger, cr *v8ov1beta1.CohCluster, state string, message string) error {
	cr.Status.State = state
	cr.Status.LastActionMessage = message
	t := time.Now().UTC()
	cr.Status.LastActionTime = fmt.Sprintf("%d-%02d-%02dT%02d:%02d:%02dZ",
		t.Year(), t.Month(), t.Day(),
		t.Hour(), t.Minute(), t.Second())

	// Update status in CR
	err := r.client.Status().Update(context.TODO(), cr)
	if err != nil {
		reqLogger.Errorf("Failed to update Coherence operator status, Error: %s", err.Error())
		return err
	}
	return nil

}

// Add finalizer to CR for cleanup when resource is deleted
func (r *ReconcileCohCluster) addFinalizer(reqLogger *zap.SugaredLogger, cr *v8ov1beta1.CohCluster) error {
	reqLogger.Infow("Adding finalizer " + finalizer)
	cr.SetFinalizers(append(cr.GetFinalizers(), finalizer))

	// Update CR
	err := r.client.Update(context.TODO(), cr)
	if err != nil {
		reqLogger.Errorf("Failed to add finalizer, Error: %s", err)
		return err
	}
	return nil
}

// Cleanup code (finalizer) when CR is deleted
func (r *ReconcileCohCluster) runFinalizer(reqLogger *zap.SugaredLogger, cr *v8ov1beta1.CohCluster) error {
	reqLogger.Infow("Running finalizer " + finalizer)
	args := []string{"delete", cr.Spec.Name, "--namespace", cr.Spec.Namespace}
	reqLogger.Infof("Deleting Coherence operator with helm, Args: %s", args)
	cmd := exec.Command("helm", args...)
	out, err := cmd.CombinedOutput()
	if err != nil {
		reqLogger.Errorf(string(out)+", Error: %s", err.Error())
		_ = r.updateStatus(reqLogger, cr, "Failed", "Coherence operator deployment deletion failed: "+string(out))
		return err
	}

	return nil
}

// Check if string is found in list
func contains(list []string, s string) bool {
	for _, v := range list {
		if v == s {
			return true
		}
	}
	return false
}

// Remove string from list
func remove(list []string, s string) []string {
	for i, v := range list {
		if v == s {
			list = append(list[:i], list[i+1:]...)
		}
	}
	return list
}
