// Copyright (c) 2020, Oracle and/or its affiliates.
// Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl.

package v1beta1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// CohClusterSpec defines the desired state of CohCluster
// +k8s:openapi-gen=true
type CohClusterSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	// Add custom validation using kubebuilder tags: https://book.kubebuilder.io/beyond_basics/generating_crd.html

	// User defined description of the the CohCluster custom resource
	Description string `json:"description"`
	// The name of the Coherence operator
	Name string `json:"name"`
	// The namespace for the Coherence operator
	Namespace string `json:"namespace"`
	// The service account for the Coherence operator
	ServiceAccount string `json:"serviceAccount,omitempty"`
	// The Kubernetes docker secret for pulling Coherence images
	// +x-kubernetes-list-type=set
	ImagePullSecrets []corev1.LocalObjectReference `json:"imagePullSecrets,omitempty"`
}

// CohClusterStatus defines the observed state of CohCluster
// +k8s:openapi-gen=true
type CohClusterStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	// Add custom validation using kubebuilder tags: https://book.kubebuilder.io/beyond_basics/generating_crd.html

	// State of the Coherence operator deployment
	State string `json:"state,omitempty"`
	// Message associated with latest action
	LastActionMessage string `json:"lastActionMessage,omitempty"`
	// Time stamp for latest action
	LastActionTime string `json:"lastActionTime,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CohCluster is the Schema for the cohclusters API
// +k8s:openapi-gen=true
// +kubebuilder:subresource:status
// +kubebuilder:resource:shortName=cc
// +genclient
// +genclient:noStatus
type CohCluster struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   CohClusterSpec   `json:"spec,omitempty"`
	Status CohClusterStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CohClusterList contains a list of CohCluster
type CohClusterList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []CohCluster `json:"items"`
}

func init() {
	SchemeBuilder.Register(&CohCluster{}, &CohClusterList{})
}
